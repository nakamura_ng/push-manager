<?php

// Auths Model
$config['loginFlag'] = array(
    'enable'  => 0,
    'disable' => 1,
);

// IosPushs Model
$config['type'] = array(
    'test' => 1,
    'live' => 2,
);

$config['status'] = array(
    'undelivered' => 0,
    'completion' => 1,
    'error' => 9,
);

$config['deleteFlag'] = array(
    'off' => 0,
    'on' => 1,
);
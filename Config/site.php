<?php

$config["siteId"] = 1;
$config["siteName"] = "Defense Witches";
$config["cookieTimeout"] = "60 minutes";
$config["passDigits"] = 6;

$config['lang'] = array(
    1 => 'ja',
    2 => 'en',
);

/* push */
$config["usePush"] = true;
$config["limit"] = 1000;
$config["apnsKeyPath"] = '/Vendor/ApnsKey/';
$config["apnsKeyFile"] = 'apns-production.pem';


/* infomation */
$config["useInfomation"] = true;
$config['infoFilePath'] = array(
    1 => '/usr/local/products/dw-info-dev/dwdata/static/jp/html/info.html', // jp
    2 => '/usr/local/products/dw-info-dev/dwdata/static/en/html/info.html', // en
);
$config['filesTxtPath'] = array(
    1 => '/usr/local/products/dw-info-dev/dwdata/static/jp/files.txt', // jp
    2 => '/usr/local/products/dw-info-dev/dwdata/static/en/files.txt', // en
);

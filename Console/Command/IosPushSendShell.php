<?php
App::import('Vendor', 'ApnsPHP/Autoload');
// ./cake IosPushSend

class IosPushSendShell extends AppShell {

    public $uses = array('IosPush', 'Token');

    public function startup(){
        //parent::startup();

        exec('ps x | grep "push-manager IosPushSend" | grep -v grep', $output, $result);
        if(count($output) > 1){
            exit();
        }

    }

    public function main() {

        $pushData = $this->IosPush->find(
            'all', array(
                'conditions' => array( 
                    'site_id' => Configure::read('siteId'),
                    'status' => Configure::read('status.undelivered'), 
                    'reserve_date <' => date('Y-m-d H:i:s'), 
                ), 
            )
        );

        $path = dirname(dirname(dirname(__FILE__))) . Configure::read('apnsKeyPath');
        $apnsKeyFile = $path . Configure::read('apnsKeyFile');

        foreach ($pushData as $val) {

            if ((int)$val['IosPush']['type'] === Configure::read('type.live')) {
                $conditions = array( 
                    'site_id' => Configure::read('siteId'),
                    'lang' => $val['IosPushCondition']['lang'], 
                );
            } else {
                $conditions = array( 
                    'site_id' => Configure::read('siteId'),
                    'app_user_id' => $val['IosPushCondition']['app_user_id'], 
                );
            }

            $count = '';
            $count = $this->Token->find(
                'count', array( 'conditions' => $conditions, 
            ));

            $cnt = $count / Configure::read('limit');

            for($i=0; $i<$cnt; $i++){
                $offset = $i * Configure::read('limit');

                $target = array();
                $target = $this->Token->find(
                    'all', array( 
                        'conditions' => $conditions, 
                        'limit' => Configure::read('limit'),
                        'offset' => $offset,
                ));


                $push = new ApnsPHP_Push(
                    ApnsPHP_Abstract::ENVIRONMENT_PRODUCTION,
                    $apnsKeyFile 
                );
                $push->connect();

                for ($n=0; $n<count($target); $n++){
                    $message = new ApnsPHP_Message($target[$n]['Token']['push_token_ios']);
                    $message->setText($val['IosPushContent']['content']);
                    $message->setExpiry(30);
                    $push->add($message);
                }

                $push->send();
                $push->disconnect();

            }

            $this->IosPush->read('id', $val['IosPush']['id']);
            $this->IosPush->set(array(
                'status' => Configure::read('status.completion')
            ));
            $this->IosPush->save();

        }

    }
}
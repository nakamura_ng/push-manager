<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    public $uses = array('Auth');

    public function beforeFilter() {
        $this->_loginCheck();
    }

    public function _loginCheck() {

        $this->Cookie = $this->Components->load('Cookie');
        $cookie = $this->Cookie->read('Person');
        $userId = $this->Session->read('Person.userId');

        if ($this->name !== 'Users') {

            if (is_null($cookie)) {
                $this->_logout();
            }

            $authData = $this->Auth->find(
                'first', array(
                    'conditions' => array( 
                        'user_id' => $userId,
                        'login' => Configure::read('loginFlag.disable')), 
                    'order' => 'id DESC', 
            ));

            if (!$authData) {
                $this->_logout();
            }

            $plain = $userId . $authData['Auth']['pass'];
            $hash = Security::hash( $plain, 'md5', true);

            if ($cookie !== $hash) {
                $this->_logout();
            }

        }

    }

    public function _logout() {
        $this->Session->delete('Person.userId');
        $this->Session->setFlash('Please login...');
        $this->redirect('/');            
    }

}

<?php
App::uses('AppController', 'Controller');
App::uses('File', 'Utility');
/**
 * Infomatinos Controller
 *
 * @property Infomatino $Infomatino
 * @property PaginatorComponent $Paginator
 */
class InfomationsController extends AppController {

    public $uses = array('Infomation');

    public function index() {
    }

    public function confirm() {
        if ($this->request->isPost()) {
            $this->Infomation->set($this->request->data);
            if($this->Infomation->validates()){
                return;
            }
        }
    }

    public function regist() {
        $langList = Configure::read('lang');
        $langId = $this->request->data['Infomation']['lang'];
        $lang = $langList[$langId];
        $type = (int)$this->request->data['Infomation']['type'];

        $siteId = Configure::read('siteId');

        $this->Infomation->set(array(
            'site_id' => $siteId,
            'type' => $type,
            'lang' => $lang,
            'date' => $this->request->data['Infomation']['date'],
            'title' => $this->request->data['Infomation']['title'], 
            'message' => $this->request->data['Infomation']['message']) 
        );

        $result = $this->Infomation->save();
        if (!$result) {
            $this->Session->setFlash('failed to update the information!');
            $this->redirect('index/type:' . $type);            
        }

        $infoList = $this->Infomation->find(
            'all', array(
                'conditions' => array( 
                    'delete_flag' => 0,
                    'type' => $type, 
                    'site_id' => $siteId, 
                    'lang' => $lang),
                'order' => 'id DESC', 

        ));

        $contents = <<<_EOD_
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Expires" content="0">
<meta name="viewport" content="width=960px, user-scalable=no">
<link rel="stylesheet" type="text/css" href="styles.css">
<title>Information - Defense Witches</title>
</head>
<body>
<header><h1>Information</h1></header>
<div id="container">
<div id="contents">

_EOD_;

        foreach ($infoList as $val) {
            $date = $val['Infomation']['date'];
            $title = $val['Infomation']['title'];
            $message = $val['Infomation']['message'];

            $contents .= <<<_EOD_

<article>
  <div class="date">$date</div>
  <div class="detail">
    <ul><li>$title</li>
        $message
    </ul>
  </div>
</article>

_EOD_;
        }

        $contents .= <<<_EOD_
</div>
</div>
</body>
</html> 
_EOD_;

        $filePath = Configure::read('infoFilePath');
        $file = new File($filePath[$langId]);
        $file->delete();
        $file->create();

        $rs   = $file->write($contents);
        $hash = $file->md5();

        $file->close();

        if (!$rs) {
            $this->Session->setFlash('upload failed info.html!');
            $this->redirect('index/type:' . $type);           
        }

        $filePath = Configure::read('filesTxtPath');
        $file = new File($filePath[$langId]);
        $body = $file->read();

        $hash = 'info.html ' . $hash . "\n";
        $body = preg_replace("/^info.html\s(.*)\n/i", $hash, $body);
        $file->delete();
        $file->create();
        $rs   = $file->write($body);

        $file->close();

        if (!$rs) {
            $this->Session->setFlash('upload failed files.txt!');
            $this->redirect('index/type:' . $type);
        }

        //$email = $this->request->data['Infomation']['email'];
        //$passwd = $this->request->data['Infomation']['passwd'];

        if ($type === Configure::read('type.live')) {
            `sh /usr/local/products/upload_dw_info_for_gae.sh`;        
        } else {
            $execRs = `sh /usr/local/products/upload_dw_info_dev_for_gae.sh`; 
        }

        $execRs = '<pre>' . $execRs . '</pre>';
        $this->set('execRs', $execRs);
    }
}




<?php
App::uses('AppController', 'Controller');
/**
 * IosPushes Controller
 *
 * @property IosPush $IosPush
 * @property PaginatorComponent $Paginator
 */
class IosPushesController extends AppController {

    public $uses = array('Token',
                         'IosPush', 
                         'IosPushCondition', 
                         'IosPushContent');

    public function condition() {
    }

    public function form() {

        $type = (int)$this->request->data['IosPush']['type'];

        if ($type === Configure::read('type.live')) {
            $langList = Configure::read('lang');
            $langId = $this->request->data['IosPushCondition']['lang'];
            $lang = $langList[$langId];

            $reserveDate = $this->request->data['IosPush']['reserve_date'];
            $reserveDate = $this->IosPush->deconstruct('reserve_date', $reserveDate);
            $lower = date('Y-m-d H:i:s', strtotime( '+10 minutes' ));

            if ($reserveDate < $lower) {
                $this->Session->setFlash('after 10minutes or more!');
                $this->redirect('condition/type:' . $type);            
            }

            $iosPushCondition = array(
                'app_user_id' => 0,
                'lang' => $lang
            );
        } else {
            $appUserId = $this->request->data['IosPushCondition']['app_user_id'];
            $reserveDate = date('Y-m-d H:i:s');

            $appUserExists = $this->Token->find(
                'count', array(
                    'conditions' => array( 
                        'app_user_id' => $appUserId)
            ));

            if (!$appUserExists) {
                $this->Session->setFlash('user ID exists!');
                $this->redirect('condition/type:' . $type);            
            }

            $iosPushCondition = array(
                'app_user_id' => $appUserId,
                'lang' => 'ja'
            );
        }

        $condition = array(
            'IosPush' => array(
                'site_id' => Configure::read('siteId'),
                'type' => $type,
                'user_id' => $this->Session->read('Person.userId'),
                'reserve_date' => $reserveDate), 
            'IosPushCondition' => $iosPushCondition
        );

        $result = $this->IosPush->saveAssociated($condition);

        if (!$result) {
            $this->Session->setFlash('failed to update the IosPushs!');
            $this->redirect('condition/type:' . $type);            
        }

        $lastId = $this->IosPush->getLastInsertID();

        if (!$lastId) {
            $this->Session->setFlash('can not get lastID!');
            $this->redirect('condition/type:' . $type);            
        }

        $this->set('lastId', $lastId);

    }

    public function regist() {
        $this->IosPushContent->set($this->request->data);
        $result = $this->IosPushContent->save();

        if (!$result) {
            $this->Session->setFlash('failed to create the pushContents!');
            $this->redirect('condition/type:' . $type);            
        }
    }

}

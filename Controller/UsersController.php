<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class UsersController extends AppController {

    public $uses = array('User', 'Auth');

    public $components = array('Cookie');

    public function index() {
    }

    public function form() {

        $postEmail = $this->request->data['User']['email'];

        $this->User->set($this->request->data);
        if(!$this->User->validates()){
            $this->Session->setFlash('not email!');
            $this->redirect('/');
        }

        $userData = $this->User->find(
            'first', array(
                'conditions' => array( 
                    'email' => $postEmail)
        ));

        if (!$userData) {
            $this->Session->setFlash('exists!');
            $this->redirect('/');
        }

        $passwd = $this->_createPasswd();
        $userId = $userData['User']['id'];
        $cookieTimeout = Configure::read('cookieTimeout');
        $timestamp = date('Y-m-d H:i:s', 
                     strtotime( '+' . $cookieTimeout ));

        $this->Auth->set(array(
            'pass' => $passwd,
            'user_id' => $userId,
            'expire' => $timestamp,
        ));
        $result = $this->Auth->save();

        if(!$result){
            $this->Session->setFlash('Please try again!');
            $this->redirect('/');
        }

        $plain = $userId . $passwd;
        $hash = Security::hash( $plain, 'md5', true);
        $this->Cookie->write( 'Person', $hash, false, $cookieTimeout);

        $email = new CakeEmail('smtp');
        $email->to($postEmail);
        $email->subject('push manager. ' . Configure::read('siteName'));
        //$email->send($passwd);

        $this->set('userId', $userId);

    }

    public function login() {

        $userId = $this->request->data['userId'];
        $passwd = $this->request->data['passwd'];

        $authData = $this->Auth->find(
            'first', array(
                'conditions' => array( 
                    'user_id' => $userId,
                    'login' => Configure::read('loginFlag.enable'), 
                    'pass' => $passwd, 
                    'expire >' => date('Y-m-d H:i:s')), 
                'order' => 'id DESC', 
        ));

        if (!$authData) {
            $this->set('userId', $userId);
            $this->Session->setFlash('password is incorrect..');
            $this->render('form');
            return false;
        }

        $this->Auth->read('id', $authData['Auth']['id']);
        $this->Auth->set(array(
            'login' => 1
        ));
        $authResult = $this->Auth->save();

        if(!$authResult){
            $this->Session->setFlash('Please try again!');
            return false;
        }

        $this->User->read('id', $userId);
        $this->User->set(array(
            'last_login' => date('Y-m-d H:i:s')
        ));
        $userResult = $this->User->save();

        if(!$userResult){
            $this->Session->setFlash('Please try again!');
            return false;
        }

        $this->Session->write('Person.userId', $userId);
        $this->redirect('/Menus/index');
    }

    public function logout() {
        $this->Cookie->delete('Person');
        $this->Session->delete('Person.userId');
        $this->Session->setFlash('logout!');
        $this->redirect('/users/index');
    }

    public function _createPasswd() {

        $passwd = '';
        $passDigits = Configure::read('passDigits');

        for ($i=0; $i<$passDigits; $i++) {
            $passwd .= mt_rand(0,9);
        }

        $exists = $this->Auth->find(
            'first', array(
                'conditions' => array( 
                    'pass' => $passwd)
        ));

        if($exists){
            $this->_createPasswd();
        }
var_dump($passwd);
        return $passwd;
    }

}

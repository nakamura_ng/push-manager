<?php
App::uses('AppModel', 'Model');
/**
 * IosPushCondition Model
 *
 * @property Push $Push
 * @property AppUser $AppUser
 */
class IosPushCondition extends AppModel {

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'push_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'lang' => array(
            'alphanumeric' => array(
                'rule' => array('alphanumeric'),
                'message' => 'only alphanumeric',
                //'allowEmpty' => true,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'app_user_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                'message' => 'only numeric.',
                //'allowEmpty' => true,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'delete_flag' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'Push' => array(
            'className' => 'Push',
            'foreignKey' => 'push_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'AppUser' => array(
            'className' => 'AppUser',
            'foreignKey' => 'app_user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
}

<?php
App::uses('AppModel', 'Model');
/**
 * IosPushContent Model
 *
 * @property Push $Push
 */
class IosPushContent extends AppModel {

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'push_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                'message' => 'numeric only.',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'content' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
                'message' => 'not empty.',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'delete_flag' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

}

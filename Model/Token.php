<?php
App::uses('AppModel', 'Model');
/**
 * Token Model
 *
 * @property Site $Site
 * @property AppUser $AppUser
 */
class Token extends AppModel {

    /**
     * Use table
     *
     * @var mixed False or table name
     */
    public $useTable = 'token';

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'site_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );
}

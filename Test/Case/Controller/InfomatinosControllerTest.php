<?php
App::uses('InfomatinosController', 'Controller');

/**
 * InfomatinosController Test Case
 *
 */
class InfomatinosControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.infomatino',
		'app.site',
		'app.auth'
	);

}

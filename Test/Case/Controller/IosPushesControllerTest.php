<?php
App::uses('IosPushesController', 'Controller');

/**
 * IosPushesController Test Case
 *
 */
class IosPushesControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.ios_push'
	);

}

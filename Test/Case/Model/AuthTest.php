<?php
App::uses('Auth', 'Model');

/**
 * Auth Test Case
 *
 */
class AuthTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.auth'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Auth = ClassRegistry::init('Auth');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Auth);

		parent::tearDown();
	}

}

<?php
App::uses('Infomatino', 'Model');

/**
 * Infomatino Test Case
 *
 */
class InfomatinoTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.infomatino',
		'app.site'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Infomatino = ClassRegistry::init('Infomatino');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Infomatino);

		parent::tearDown();
	}

}

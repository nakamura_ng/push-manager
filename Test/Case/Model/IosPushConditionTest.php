<?php
App::uses('IosPushCondition', 'Model');

/**
 * IosPushCondition Test Case
 *
 */
class IosPushConditionTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.ios_push_condition',
		'app.push',
		'app.app_user'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->IosPushCondition = ClassRegistry::init('IosPushCondition');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->IosPushCondition);

		parent::tearDown();
	}

}

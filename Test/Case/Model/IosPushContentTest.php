<?php
App::uses('IosPushContent', 'Model');

/**
 * IosPushContent Test Case
 *
 */
class IosPushContentTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.ios_push_content',
		'app.push'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->IosPushContent = ClassRegistry::init('IosPushContent');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->IosPushContent);

		parent::tearDown();
	}

}

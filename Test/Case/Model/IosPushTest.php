<?php
App::uses('IosPush', 'Model');

/**
 * IosPush Test Case
 *
 */
class IosPushTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.ios_push'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->IosPush = ClassRegistry::init('IosPush');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->IosPush);

		parent::tearDown();
	}

}

<?php
/**
 * IosPushConditionFixture
 *
 */
class IosPushConditionFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'push_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index'),
		'app_user_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 10),
		'lang' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 3, 'key' => 'index'),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'delete_flag' => array('type' => 'integer', 'null' => false, 'default' => '0', 'length' => 3),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'idx_1' => array('column' => 'push_id', 'unique' => 0),
			'idx_2' => array('column' => 'lang', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'push_id' => 1,
			'app_user_id' => 1,
			'lang' => 1,
			'created' => '2013-12-14 04:01:35',
			'modified' => '2013-12-14 04:01:35',
			'delete_flag' => 1
		),
	);

}

<?php
/**
 * IosPushFixture
 *
 */
class IosPushFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'ios_pushs';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'site_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'type' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 3, 'key' => 'index'),
		'status' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 3),
		'reserve_date' => array('type' => 'datetime', 'null' => false, 'default' => null, 'key' => 'index'),
		'user_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10),
		'start_date' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'end_date' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'delete_flag' => array('type' => 'integer', 'null' => false, 'default' => '0', 'length' => 3),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'idx_1' => array('column' => 'reserve_date', 'unique' => 0),
			'idx_2' => array('column' => array('type', 'site_id'), 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'site_id' => 1,
			'type' => 1,
			'status' => 1,
			'reserve_date' => '2013-12-14 03:58:39',
			'user_id' => 1,
			'start_date' => '2013-12-14 03:58:39',
			'end_date' => '2013-12-14 03:58:39',
			'created' => '2013-12-14 03:58:39',
			'modified' => '2013-12-14 03:58:39',
			'delete_flag' => 1
		),
	);

}

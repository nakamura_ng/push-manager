<?php
/**
 * TokenFixture
 *
 */
class TokenFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'token';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'site_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index'),
		'app_user_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'push_token_ios' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'push_token_android' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'lang' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 5, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => '0000-00-00 00:00:00'),
		'updated' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'deleted' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'SEARCH_INDEX' => array('column' => array('site_id', 'push_token_ios', 'push_token_android', 'lang', 'deleted'), 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'site_id' => 1,
			'app_user_id' => 1,
			'push_token_ios' => 'Lorem ipsum dolor sit amet',
			'push_token_android' => 'Lorem ipsum dolor sit amet',
			'lang' => 'Lor',
			'created' => '2013-12-24 22:11:49',
			'modified' => '2013-12-24 22:11:49',
			'updated' => '2013-12-24 22:11:49',
			'deleted' => '2013-12-24 22:11:49'
		),
	);

}

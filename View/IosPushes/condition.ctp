
<?php


echo $this->Form->create(false, array('action'=>'form'));
echo $this->Form->hidden('IosPush.type', array('value'=> $this->request->params['named']['type'] ));

if ($this->request->params['named']['type'] == Configure::read('type.live')) {
    echo $this->Form->input('IosPushCondition.lang', array('type' => 'select', 
                                                                     'default' => 'jp', 
                                                                     'options' => Configure::read('lang')));
    echo $this->Form->input('IosPush.reserve_date');

} else {
    echo $this->Form->input('IosPushCondition.app_user_id', array('type' => 'text'));
}

echo $this->Form->end('ok');

?>




<!DOCTYPE html>
<html>
<head>
    <?php echo $this->Html->charset(); ?>
    <title>
        <?php echo Configure::read('siteName')?>
    </title>
    <?php
        echo $this->Html->css('cake.generic');
        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
    ?>
<link rel="stylesheet" type="text/css" href="/css/custom.css" />
<html xmlns="http://www.w3.org/1999/xhtml" lang="ja" xml:lang="ja">
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<script >
    clickMenu = function(menu) {
        var getEls = document.getElementById(menu).getElementsByTagName("LI");
        var getAgn = getEls;

        for (var i=0; i<getEls.length; i++) {
                getEls[i].onclick=function() {
                    for (var x=0; x<getAgn.length; x++) {
                    getAgn[x].className=getAgn[x].className.replace("unclick", "");
                    getAgn[x].className=getAgn[x].className.replace("click", "unclick");
                    }
                if ((this.className.indexOf('unclick'))!=-1) {
                    this.className=this.className.replace("unclick", "");;
                    }
                    else {
                    this.className+=" click";
                    }
                }
            }
        }

</script>
</head>
<body>

<div id="wrapper">
    <div id="customHeader">
        <h1><?php echo Configure::read('siteName')?></h1>
        <div id="menu">
            <li onmouseover="document.getElementById('n_pop').style.visibility='visible'" 
                onmouseout="document.getElementById('n_pop').style.visibility='hidden'">
                Infomation
                <ul id="n_pop">
                    <li><a href="/Infomation/index/">TEST</a></li>
                    <li><a href="/Infomation/index/">LIVE</a></li>
                </ul>
            </li>

            <li onmouseover="document.getElementById('f_pop').style.visibility='visible'" 
                onmouseout="document.getElementById('f_pop').style.visibility='hidden'">
                Push
                <ul id="f_pop">
                    <li><a href="/IosPush/form/">TEST</a></li>
                    <li><a href="/IosPush/form/">LIVE</a></li>
                </ul>
            </li>
        </div>
    </div>

    <div id="container">
        <div id="contents">
            <?php echo $this->Session->flash(); ?>
            <?php echo $this->fetch('content'); ?>
        </div>
    </div>

    <div id="customFooter">

    </div>

</div>

</body>
</html>
